FROM openjdk:8u151-jre-alpine3.7

COPY target/firstkube-hello-0.0.1-SNAPSHOT.jar /tmp/app.jar

EXPOSE 8080
ENTRYPOINT java /tmp/app.jar
