package org.okcoder.kubernetes.firstkubehello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstkubeHelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstkubeHelloApplication.class, args);
	}
}
