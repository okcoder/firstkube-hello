package org.okcoder.kubernetes.firstkubehello;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/")
	public String hello() {
		return "hello ".concat(System.getenv("HOSTNAME").concat(" v1"));
	}

	@GetMapping("/env")
	public Map<String, String> env() {
		return System.getenv();
	}

	@GetMapping("sleep/{sleep}")
	public int sleep(@PathVariable int sleep) {
		try {
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
		}
		return sleep;
	}

	@GetMapping("run/{sleep}")
	public int run(@PathVariable int sleep) {
		LocalDateTime to = LocalDateTime.now().plus(sleep, ChronoUnit.MILLIS);
		while (LocalDateTime.now().isBefore(to)) {
		}
		return sleep;
	}
	

	@GetMapping("sleepRandom/{sleep}")
	public int sleepRandom(@PathVariable int sleep) {
		sleep = new Random().nextInt(sleep);
		try {
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
		}
		return sleep;
	}

	@GetMapping("runRandom/{sleep}")
	public int runRandom(@PathVariable int sleep) {
		sleep = new Random().nextInt(sleep);
		LocalDateTime to = LocalDateTime.now().plus(sleep, ChronoUnit.MILLIS);
		while (LocalDateTime.now().isBefore(to)) {
		}
		return sleep;
	}
	
}
