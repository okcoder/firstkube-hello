package org.okcoder.kubernetes.firstkubehello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FirstkubeHelloApplicationTests {

	@Test
	public void contextLoads() {
	}

}
